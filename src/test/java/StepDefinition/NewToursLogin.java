package StepDefinition;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.io.File;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.NewTours.CommonMethods.NewToursMethods;
import com.NewTours.Execelutilities.ExcelUtilities;
import com.NewTours.Weblocators.NewToursHomepageLocators;
import com.NewTours.Weblocators.NewToursLocators;

public class NewToursLogin {
	
	static WebDriver driver;
	@Given("^User is on login Page$")
	public void user_is_on_login_Page() throws Throwable {
		File dir1 = new File("TC1_Screenshots");  //Specify the Folder name here
		dir1.mkdir( ); 
		//System.setProperty("webdriver.chrome.driver","C:\\myworkspace\\CucumberExample\\Drivers\\chromedriver.exe");
		System.setProperty("webdriver.gecko.driver","C:\\myworkspace\\CucumberExample\\Drivers\\geckodriver.exe");
		//driver = new ChromeDriver();
		driver = new FirefoxDriver();
	      driver.manage().window().maximize();
	      System.out.println("Browser initialized");
	      ExcelUtilities.writeresult("C:\\myworkspace\\CucumberExample\\TestData\\Results.xlsx", 1, 0, 0, "Step1");
	      ExcelUtilities.writeresult("C:\\myworkspace\\CucumberExample\\TestData\\Results.xlsx", 1, 1, 0, "Browser_Initialized");
	      ExcelUtilities.writeresult("C:\\myworkspace\\CucumberExample\\TestData\\Results.xlsx", 1, 2, 0, "Passed");

	      driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			
	}

	@When("^User Navigate to LogIn Page$")
	public void user_Navigate_to_LogIn_Page() throws Throwable {
		try {
			driver.get(ExcelUtilities.getURL("C:\\myworkspace\\CucumberExample\\TestData\\TestData.xlsx", 1, 0, "URL"));
			System.out.println("Login page is displayed");
			NewToursMethods.captureScreenShot(driver,"Step2_Login_Page_Displayed","TC1_Screenshots");
			ExcelUtilities.writeresult("C:\\myworkspace\\CucumberExample\\TestData\\Results.xlsx", 1, 0, 1, "Step2_Login_page_displayed");
		}  catch(Exception e) {
	          e.printStackTrace();
	          System.out.println("Exception undergone");
	  }
		}

	@When("^User enters UserName and Password$")
	public void user_enters_UserName_and_Password() throws Throwable {
	
		NewToursLocators.txt_userName(driver).sendKeys(ExcelUtilities.getData("C:\\myworkspace\\CucumberExample\\TestData\\TestData.xlsx",0, 1, 1));
		System.out.println("Username entered");
		NewToursLocators.txt_password(driver).sendKeys(ExcelUtilities.getData("C:\\myworkspace\\CucumberExample\\TestData\\TestData.xlsx",0, 1, 2));
		System.out.println("Password entered");
		NewToursMethods.captureScreenShot(driver,"Step3_Username_Password_entered","TC1_Screenshots");
	}
	
	@When("^click submit$")
	public void click_submit() throws Throwable {
	
		NewToursLocators.btn_SignIn(driver).click();
		Thread.sleep(10000);
		System.out.println("Sign in button clicked");
		ExcelUtilities.writeresult("C:\\myworkspace\\CucumberExample\\TestData\\Results.xlsx", 2, 0, 1, "Step3_Signin_button_clicked");
	}
	

	@Then("^Message displayed Login Successfully$")
	public void message_displayed_Login_Successfully() throws Throwable {
	System.out.println("login successfully displayed");
	}	
	@Given("^User is on Flight Booking Page$")
	public void user_is_on_Flight_Booking_Page() throws Throwable {
		System.out.println("on Flight Booking Page");
	    
	}

	@When("^User Navigate to Flight Booking Page$")
	public void user_Navigate_to_Flight_Booking_Page() throws Throwable {
		
		NewToursMethods.captureScreenShot(driver,"Step4_Homepage_Displayed","TC1_Screenshots");
		ExcelUtilities.writeresult("C:\\myworkspace\\CucumberExample\\TestData\\Results.xlsx", 3, 0, 1, "Step4_Home_Page_Displayed");
	    
	}

	@When("^Selecting the trip type$")
	public void selecting_the_trip_type() throws Throwable {
		 String TripType = ExcelUtilities.getData("C:\\myworkspace\\CucumberExample\\TestData\\TestData.xlsx",1,1,0);
	
		
			if(TripType == "Round Trip"){	  
			driver.findElement( By.xpath(NewToursHomepageLocators.RoundTrip_XPATH)).click();
			}	
			else{	  
			driver.findElement(By.xpath(NewToursHomepageLocators.Oneway_XPATH)).click();
			}
	}

	@When("^Selecting the passengers$")
	public void selecting_the_passengers() throws Throwable {
	
		Select Dropdown = new Select(driver.findElement(By.name(NewToursHomepageLocators.Passengers_NAME)));
		Dropdown.selectByVisibleText(ExcelUtilities.getData("C:\\myworkspace\\CucumberExample\\TestData\\TestData.xlsx",1,1,1));
	   
	}

	@When("^Selecting the departing from$")
	public void selecting_the_departing_from() throws Throwable {
		Select Departingfrom = new Select(driver.findElement(By.name(NewToursHomepageLocators.Departingfrom_NAME)));
		Departingfrom.selectByVisibleText(ExcelUtilities.getData("C:\\myworkspace\\CucumberExample\\TestData\\TestData.xlsx",1,1,2)); 
	   
	}

	@When("^Selecting On Month$")
	public void selecting_On_Month() throws IOException  {
		Select Month = new Select(driver.findElement(By.name(NewToursHomepageLocators.OnMonth_NAME)));
		Month.selectByVisibleText(ExcelUtilities.getData("C:\\myworkspace\\CucumberExample\\TestData\\TestData.xlsx",1,1,3));
	}

	@When("^Selecting On Date$")
	public void selecting_On_Date() throws Throwable {
		Select Ondate = new Select(driver.findElement(By.name(NewToursHomepageLocators.OnDate_NAME)));
		Ondate.selectByVisibleText(ExcelUtilities.getData("C:\\myworkspace\\CucumberExample\\TestData\\TestData.xlsx",1,1,4));
	    
	}

	@When("^Selecting Arriving in$")
	public void selecting_Arriving_in() throws Throwable {
		Select arrivein = new Select(driver.findElement(By.name(NewToursHomepageLocators.ArrivingIn_NAME)));
		arrivein.selectByVisibleText(ExcelUtilities.getData("C:\\myworkspace\\CucumberExample\\TestData\\TestData.xlsx",1,1,5));
	    
	}

	@When("^Selecting Returning Month$")
	public void selecting_Returning_Month() throws Throwable {
		Select returnmonth = new Select(driver.findElement(By.name(NewToursHomepageLocators.ReturningMonth_NAME)));
		returnmonth.selectByVisibleText(ExcelUtilities.getData("C:\\myworkspace\\CucumberExample\\TestData\\TestData.xlsx",1,1,6)); 

	  
	}

	@When("^Selecting Returning Date$")
	public void selecting_Returning_Date() throws Throwable {
		Select returndate = new Select(driver.findElement(By.name(NewToursHomepageLocators.ReturningDate_NAME)));
		returndate.selectByVisibleText(ExcelUtilities.getData("C:\\myworkspace\\CucumberExample\\TestData\\TestData.xlsx",1,1,7));
		     
	    
	}

	@When("^Selecting the service class$")
	public void selecting_the_service_class() throws Throwable {
		String ClassType = ExcelUtilities.getData("C:\\myworkspace\\CucumberExample\\TestData\\TestData.xlsx",1,1,8);
		  
		if(ClassType == "Business class"){		  
		driver.findElement(By.cssSelector(NewToursHomepageLocators.BusinessClass_CSSSELECTOR)).click();
			  }
			  
		else if(ClassType == "Economy"){		  
		driver.findElement(By.cssSelector(NewToursHomepageLocators.EconomyClass_CSSSELECTOR)).click();
			  }
			  
		else{		  
		driver.findElement(By.cssSelector(NewToursHomepageLocators.FirstClass_CSSSELECTOR)).click();
			 }
	   
	}

	@When("^Selecting the airline$")
	public void selecting_the_airline() throws Throwable {
		Select airline = new Select(driver.findElement(By.name(NewToursHomepageLocators.Airline_NAME)));
	      
		airline.selectByVisibleText(ExcelUtilities.getData("C:\\myworkspace\\CucumberExample\\TestData\\TestData.xlsx",1,1,9));
			  
	    
	}

	@When("^Capturing the screenshot$")
	public void capturing_the_screenshot() throws Throwable {
		NewToursMethods.captureScreenShot(driver, "Step5_First_page_details_filled","TC1_Screenshots");
		ExcelUtilities.writeresult("C:\\myworkspace\\CucumberExample\\TestData\\Results.xlsx", 4, 0, 1, "Step5_First_Page_Details_filled");
		    

	    
	}

	@When("^Clicking on continue$")
	public void clicking_on_continue() throws Throwable {
		driver.findElement(By.name(NewToursHomepageLocators.Continue_NAME)).click();   

	    
	}

	@When("^Clicking on continue in second page$")
	public void clicking_on_continue_in_second_page() throws Throwable {
		driver.findElement(By.name(NewToursHomepageLocators.Continue1_NAME)).click();
	      
	    
	}

	@When("^Entering the first name$")
	public void entering_the_first_name() throws Throwable {
		driver.findElement(By.name(NewToursHomepageLocators.FirstName_NAME)).sendKeys(ExcelUtilities.getData("C:\\myworkspace\\NewToursProject\\TestData\\TestData.xlsx",1,1,10));

	    
	}

	@When("^Entering the last name$")
	public void entering_the_last_name() throws Throwable {
		driver.findElement(By.name(NewToursHomepageLocators.LastName_NAME)).sendKeys(ExcelUtilities.getData("C:\\myworkspace\\NewToursProject\\TestData\\TestData.xlsx",1,1,11));

	    
	}

	@When("^Entering the Number$")
	public void entering_the_Number() throws Throwable {
		driver.findElement(By.name(NewToursHomepageLocators.Number_NAME)).sendKeys(ExcelUtilities.getData("C:\\myworkspace\\NewToursProject\\TestData\\TestData.xlsx",1,1,12));
	      
	    
	}

	@When("^Clicking on secure purchase$")
	public void clicking_on_secure_purchase() throws Throwable {
		driver.findElement(By.name(NewToursHomepageLocators.Securepurchase_NAME)).click();
	    
	}

	@When("^Checking the confirmation message$")
	public void checking_the_confirmation_message() throws Throwable {
		String ConfirmTitle;  
		ConfirmTitle = "Flight Confirmation: Mercury Tours";  
		boolean flag = false;  
		if(driver.getTitle().equalsIgnoreCase(ConfirmTitle)){	  
		flag = true;
		System.out.println("Flight Booked Successfully");
		NewToursMethods.captureScreenShot(driver, "Step8_Flight_Book_Confirmation_Page","TC1_Screenshots");
		 //NewToursMethods.captureScreenShot(driver, "Step8_Flight_Book_Confirmation_Page","TC1_Screenshots");
		ExcelUtilities.writeresult("C:\\myworkspace\\CucumberExample\\TestData\\Results.xlsx", 1, 2, 0, "Passed"); 
		ExcelUtilities.writeresult("C:\\myworkspace\\CucumberExample\\TestData\\Results.xlsx", 7, 0,1, "Step8_Flight_Booking_Confirmed"); 
		}
		Assert.assertTrue(flag, "Page title is not matching with expected"); 
		}
	    
	

	@Then("^Message displayed Flight Booked Successfully$")
	public void message_displayed_Flight_Booked_Successfully() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
	}
    
}



